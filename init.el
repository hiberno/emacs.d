;;; init.el --- Emacs configuration file

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This Emacs configuration is modular, which means that this init.el
;; merely requires the configuration files residing in ~/.emacs.d/init/

;;; Code:

(message "Emacs is starting. Configuration files are loaded.")

;; Add the directory containing the configuration files to the load-path
(add-to-list 'load-path (expand-file-name "init" user-emacs-directory))

;; Require the wanted configuration files
(message "Loading the basics ...")
(require 'init-basics)
(require 'init-appearance)

(message "Loading mail setup ...")
(require 'init-mail)

(message "Loading the packages ...")
(require 'init-package-anzu )
(require 'init-package-company)
(require 'init-package-deft)
(require 'init-package-define-word)
(require 'init-package-dictcc)
(require 'init-package-dired)
(require 'init-package-elfeed)
(require 'init-package-expand-region)
(require 'init-package-flycheck)
(require 'init-package-flyspell)
(require 'init-package-helm)
(require 'init-package-highlight-symbol)
(require 'init-package-magit)
(require 'init-package-mingus)
(require 'init-package-org)
(require 'init-package-rainbow-delimiters)
(require 'init-package-scpaste)
(require 'init-package-smartparens)
(require 'init-package-twittering)
(require 'init-package-undo-tree)
(require 'init-package-unfill)
(require 'init-package-uniquify)
(require 'init-package-weechat)
(require 'init-package-whitespace-cleanup)

(message "Loading custom configurations ...")
(require 'init-custom-editing)
(require 'init-custom-functions)
(require 'init-custom-keybindings)
(require 'init-misc)

(message "Loading language configurations ...")
(require 'init-language-c)
(require 'init-language-chicken)
(require 'init-language-clojure)
(require 'init-language-csv)
(require 'init-language-haskell)
(require 'init-language-latex)
(require 'init-language-lisp)
(require 'init-language-markdown)
(require 'init-language-nix)
(require 'init-language-ruby)
(require 'init-language-rust)

(message "All done. Enjoy.")

;;; init.el ends here
