# emacs.d #

Configuring Emacs is a must. I once did my configuration in an
`orgmode` file with `req-package`, `cask` and `pallet`, but that was
too complex in the end.

I simplified my configuration by sticking to `use-package` and a
modularized set of simple `.el` files.

# Inspirations #

This configuration is heavily inspired by

* Bozhidar Batsov's infamous [Prelude](https://github.com/bbatsov/prelude)
* Jack Henahan's [emacs.d](https://github.com/jhenahan/emacs.d)
* edvorg's [emacs-configs](https://github.com/edvorg/emacs-configs)
* As well as others I do not remember. Sorry and thanks!
