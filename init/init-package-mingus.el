;;; init-package-mingus.el --- MPD in Emacs

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file sets up Mingus, the Emacs MPD interface

;;; Code:

(use-package mingus
  :load-path "additional-packages/mingus/"
  :bind ("C-x m" . mingus))

(provide 'init-package-mingus)

;;; init-package-minugs.el ends here
