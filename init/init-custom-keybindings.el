;;; init-custom-keybindings.el --- Various custom keybindings

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(bind-key "C-o" 'isearch-occur isearch-mode-map)

(bind-key "C-m" 'newline-and-indent)
(bind-key "M-<space>" 'cycling-spacing)
(bind-key "C-M-<backspace>" 'hiberno/kill-back-to-indentation)
(bind-key "C-o" 'hiberno/open-line-with-reindent)
(bind-key "C-M-=" 'hiberno/indent-region-or-whole-buffer)
(bind-key "C-M-<return>" 'hiberno/insert-line-above)
(bind-key "C-c t" 'comment-or-uncomment-region)
(bind-key "C-c C-z" 'replace-string)
(bind-key "C-z" 'query-replace)
(bind-key "M-z" 'hiberno/zap-to-isearch isearch-mode-map)
(bind-key "C-<return>" 'hiberno/isearch-exit-other-end)
(bind-key "C-M-w" 'hiberno/isearch-yank-symbol isearch-mode-map)
(bind-key* "C-:" 'browse-url)

(provide 'init-custom-keybindings)

;;; init-custom-keybindings.el ends here
