;;; init-language-markdown --- Settings for editing Markdown

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package markdown-mode
  :ensure t
  :pin melpa-stable
  :config (progn
	    (smartparens-mode +1)
	    (add-to-list 'auto-mode-alist '(".\\md" . markdown-mode))
	    (add-to-list 'auto-mode-alist '("README.md" . gfm-mode))))

(provide 'init-language-markdown)

;;; init-language-markdown.el ends here
