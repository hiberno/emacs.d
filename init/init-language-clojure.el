;;; init-language-clojure --- Settings and packages for editing Clojure code

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package cider
  :ensure t
  :pin melpa-stable
  :config (progn
	    (add-hook 'cider-mode-hook 'cider-turn-on-eldoc-mode)
	    (add-hook 'cider-repl-mode-hook 'rainbow-delimiters-mode)
	    (add-hook 'cider-repl-mode-hook (lambda () (smartparens-strict-mode t)))
	    (setq nrepl-hide-special-buffers t
		  cider-repl-pop-to-buffer-on-connect nil
		  cider-repl-use-clojure-font-lock t
		  cider-repl-wrap-history t
		  cider-repl-history-file (expand-file-name ".cider-repl.history" user-emacs-directory)
		  cider-repl-history-size 1000)))

(use-package clojure-mode
  :ensure t
  :pin melpa-stable
  :config (progn
	    (add-hook 'clojure-mode-hook 'subword-mode)
	    (add-hook 'clojure-mode-hook 'rainbow-delimiters-mode)
	    (add-hook 'clojure-mode-hook (lambda () (smartparens-strict-mode t)))))

(provide 'init-language-clojure)

;;; init-language-clojure.el ends here
