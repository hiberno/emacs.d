;;; init-package-whitespace-cleanup.el --- Clean up whitespace errors

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package whitespace-cleanup-mode
  :ensure t
  :pin melpa-stable
  :config (global-whitespace-cleanup-mode t))

(provide 'init-package-whitespace-cleanup)

;;; init-package-whitespace-cleanup.el ends here
