;;; init-package-org --- Your life in plain text.

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package org
  :ensure t
  :pin org
  :config (progn
	    (use-package org-bullets
	      :ensure t
	      :pin melpa-stable)
	    (add-hook 'org-mode-hook
		      '(lambda ()
			 (org-bullets-mode t)
			 (setq mode-name " ꙮ ")
			 (org-babel-do-load-languages
			  'org-babel-load-languages
			  '((emacs-lisp . t)
			    (C . t)))))
	    (setq org-log-done t
		  org-completion-use-ido t
		  org-edit-timestamp-down-means-later t
		  org-fast-tag-selection-single-key 'expert
		  org-export-kill-product-buffer-when-displayed t
		  org-use-speed-commands t
		  org-tags-column 80
		  org-todo-keywords '((sequence "TODO(t)" "APPOINTMENT(a)" "WAIT(w)" "POSTPONED(p)" "IN PROGRESS(i)" "VERIFY(v)" "READ(r)" "|" "DONE(d)" "CANCELED(c)" "DELEGATED(e)"))
		  org-src-fontify-natively t
		  org-src-tab-acts-natively t

		  org-agenda-start-on-weekday nil
		  org-agenda-span 14
		  org-agenda-include-diary t
		  org-agenda-window-setup 'current-window
		  org-agenda-files (list "~/org/todo/")

		  org-journal-dir "~/org/journal"
		  org-journal-date-format "%D, %H:%M")
	    (bind-key* "C-c l" 'org-store-link)
	    (bind-key* "C-c a" 'org-agenda)
	    (bind-key* "C-c b" 'org-iswitch)))

(provide 'init-package-org)

;;; init-package-org.el ends here
