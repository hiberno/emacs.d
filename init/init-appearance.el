;;; init-appearance.el --- Beautifying Emacs

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file contains stuff that makes Emacs a beauty.

;;; Code:

(use-package material-theme
  :ensure t
  :pin melpa
  :if window-system
  :config (progn (load-theme 'material t)
		 (enable-theme 'material)))

(use-package smart-mode-line
  :ensure t
  :pin melpa-stable
  :config (progn
	    (setq sml/theme nil
		  sml/shorten-directory t
		  sml/name-width 32)
	    (sml/setup)))

(set-default-font "Fira Mono 7")

(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)

(use-package golden-ratio
  :ensure t
  :diminish golden-ratio-mode
  :config (golden-ratio-mode 1))

(use-package smooth-scrolling
  :ensure t
  :init (setq
	  scroll-margin 1
	  scroll-conservatively 0
	  scroll-up-aggressively 0.01
	  scroll-down-aggressively 0.01
	  scroll-step 1
	  scroll-preserve-screen-position 1
	  auto-window-vscroll nil))

(provide 'init-appearance)

;;; init-appearance.el ends here
