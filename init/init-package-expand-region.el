;;; init-package-expand-region --- Marking by semantic units

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package expand-region
  :ensure t
  :pin melpa-stable
  :bind ("C-#" . er/expand-region))

(provide 'init-package-expand-region)

;;; init-package-expand-region.el ends here
