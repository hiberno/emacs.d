;;; init-language-ruby --- Settings for editing Ruby code

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package inf-ruby
  :ensure t
  :pin melpa-stable)

(use-package yari
  :ensure t
  :pin melpa-stable)

(use-package ruby-tools
  :ensure t
  :pin melpa-stable
  :config (progn
	    (add-to-list 'auto-mode-alist '("\\.rake\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("Rakefile\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("\\.gemspec\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("\\.ru\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("Gemfile\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("Guardfile\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("Capfile\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("\\.thor\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("\\.rabl\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("Thorfile\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("Vagrantfile\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("\\.jbuilder\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("Podfile\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("\\.podspec\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("Puppetfile\\'" . ruby-mode))
	    (add-to-list 'auto-mode-alist '("Berksfile\\'" . ruby-mode))
	    (eval-after-load 'ruby-mode
	      '(progn
		 (smartparens-mode +1)
		 (inf-ruby-minor-mode +1)
		 (ruby-tools-mode +1)
		 (subword-mode +1)))))

(provide 'init-language-ruby)

;;; init-language-ruby.el ends here
