;;; init-package-twittering.el --- Twitter for Emacs

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file sets up Twitter for Emacs


;;; Code:

(use-package twittering-mode
  :ensure t
  :pin melpa
  :config (progn (setq twittering-cert-file "/etc/ssl/certs/ca-certificates.crt"
		       twittering-use-master-password t)
		 (bind-key* "C-x t" 'twit)
		 (bind-key "C-c f" 'twittering-favorite twittering-mode-map)))

(provide 'init-package-twittering)

;;; init-package-twittering.el ends here
