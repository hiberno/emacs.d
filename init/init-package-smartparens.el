;;; init-package-smartparens.el --- Working with pairs

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package smartparens
  :ensure t
  :pin melpa-stable
  :init (progn
	  (require 'smartparens-config)
	  (setq sp-base-key-bindings 'paredit
		sp-autoskip-closing-pair 'always
		sp-hybrid-kill-entire-symbol)
	  (sp-use-smartparens-bindings)
	  (show-smartparens-global-mode +1))
  :bind ("M-r" . sp-raise-sexp))

(provide 'init-package-smartparens)

;;; init-package-smartparens.el ends here
