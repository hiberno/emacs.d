;;; init-language-c --- Settings for editing C/C++ code

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package company-c-headers
  :ensure t
  :pin melpa-stable
  :config (progn
	    (add-to-list 'company-backends 'company-c-headers)
	    (add-hook 'cc-mode (progn
				 (smartparens-mode +1)
				 (setq c-basic-offset 4
				       c-indentation-style "bsd")))))

(provide 'init-language-c)

;;; init-language-c.el ends here
