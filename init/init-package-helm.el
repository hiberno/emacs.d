;;; init-package-helm.el --- All you need is Helm

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file sets up the "eierlegende Wollmichsau" called Helm.

;;; Code:

(use-package helm-core
  :ensure t
  :pin melpa-stable
  :defer t
  :bind (("C-x C-f" . helm-find-files)
	 ("M-x" . helm-M-x)
	 ("C-x b" . helm-buffers-list)
	 ("C-x C-b" . ibuffer)
	 ("C-c h" . helm-mini)
	 ("M-r" . helm-resume)
	 ("M-y" . helm-show-kill-ring)
	 ("C-M-o" . helm-occur)
	 ("C-c i" . helm-imenu)
	 ("C-c f" . helm-apropos)
	 ("C-c r" . helm-info-emacs))
  :config (progn
	    (use-package helm
	      :ensure t
	      :pin melpa-stable)
	    (setq helm-recentf-fuzzy-match t
		  helm-buffers-fuzzy-matching t
		  helm-M-x-fuzzy-match t
		  helm-imenu-fuzzy-match t)
	    (use-package helm-config)))

(use-package helm-ag
  :ensure t
  :pin melpa-stable
  :defer t
  :if (executable-find "ag")
  :bind ("C-x C-a" . helm-ag))
(use-package helm-backup
  :ensure t
  :pin melpa-stable
  :defer t
  :bind ("C-c b" . helm-backup)
  :config (progn
	    (add-hook 'after-save-hook 'helm-backup-versioning)
	    (setq helm-backup-path (expand-file-name "backup" user-emacs-directory))))

(provide 'init-package-helm)

;;; init-package-helm.el ends here
