;;; init-custom-functions.el --- Various custom functions

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(defun hiberno/isearch-yank-symbol ()
  "Put symbol at current point into search string."
  (interactive)
  (let ((sym (symbol-at-point)))
    (if sym
	(progn
	  (setq isearch-regexp t
		isearch-string (concat "\\_<" (regexp-quote (symbol-name sym)) "\\_>")
		isearch-message (mapconcat 'isearch-text-char-description isearch-string "")
		isearch-yank-flag t))
      (ding)))
  (isearch-search-and-update))

(defun hiberno/zap-to-isearch (rbeg rend)
  "Kill the region between the mark and the closest portion ofthe
    isearch match string. The behaviour is meant to be analogousto
    zap-to-char; let's call it zap-to-isearch. The deleted
    regiondoes not include the isearch word. This is meant to be
    bound onlyin isearch mode. The point of this function is that
    oftentimesyou want to delete some portion of text, one end of
    which happensto be an active isearch word. The observation to
    make is that ifyou use isearch a lot to move the cursor
    around (as you should,it is much more efficient than using the
    arrows), it happens alot that you could just delete the active
    region between the markand the point, not include the isearch
    word."
  (interactive "r")
  (when (not-mark-active)
    (error "Mark is not active"))
  (let* ((isearch-bounds (list isearch-other-end (point)))
	 (ismin (apply 'min isearch-bounds))
	 (ismax (apply 'max isearch-bounds)))
    (if (< (mark) ismin)
	(kill-region (mark) ismin)
      (if (> (mark) ismax)
	  (kill-region ismax (mark))
	(error "Internal error in isearch kill function.")))
    (isearch-exit)))


(defun hiberno/isearch-exit-other-end (rbeg rend)
  "Exit isearch, but at the other end of the search string.
    This is useful when followed by an immediate kill."
  (interactive "r")
  (isearch-exit)
  (goto-char isearch-other-end))

(defun hiberno/delete-this-file ()
  "Delete the current file, and kill the buffer."
  (interactive)
  (or (buffer-file-name) (error "No file is currently being edited."))
  (when (yes-or-no-p (format "Really delete '%s'?"
                             (file-name-nondirectory buffer-file-name)))
    (delete-file (buffer-file-name))
    (kill-this-buffer)))

(defun hiberno/kill-back-to-indentation ()
  "Kill from point back to the first non-whitespace character on the line."
  (interactive)
  (let ((prev-pos (point)))
    (back-to-indentation)
    (kill-region) (point) prev-pos))

(defun hiberno/open-line-with-reindent (n)
  "A version `open-line' which reindents the start and end positions.
If there is a fill prefix and/or a `left-margin', insert them on the
newline if the the line would have been blank.
With arg N, insert N newlines."
  (interactive "*p")
  (let* ((do-fill-prefix (and fill-prefix (bolp)))
         (do-left-margin (and (bolp) (> (current-left-margin) 0)))
         (loc (point-marker))
         (abbrev-mode nil))
    (delete-horizontal-space t)
    (newline n)
    (indent-according-to-mode)
    (when (eolp)
      (delete-horizontal-space t))
    (goto-char loc)
    (while (> n 0)
      (cond ((bolp)
             (if do-left-margin (indent-to (current-left-margin)))
             (if do-fill-prefix (insert-and-inherit fill-prefix))))
      (forward-line 1)
      (setq n (1- n)))
    (goto-char loc)
    (end-of-line)
    (indent-according-to-mode)))

(defun hiberno/insert-line-above ()
  "Inserts line above point."
  (interactive)
  (move-beginning-of-line nil)
  (newline-and-indent)
  (forward-line -1)
  (indent-according-to-mode))

(defun hiberno/indent-region-or-whole-buffer()
  "Indent the currently marked region or, if not, the whole buffer."
  (interactive)
  (save-excursion
    (if (region-active-p)
        (progn
          (indent-region (region-beginning) (region-end))
          (message "Indented selected region."))
      (progn
        (indent-region (point-min) (point-max))
        (message "Indented buffer.")))))

(provide 'init-custom-functions)

;;; init-custom-functions.el ends here
