;;; init-custom-editing.el --- Various settings with regard to editing

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(setq-default
 blink-cursor-delay 0
 blink-cursor-interval 0.4
 bookmark-default-file (expand-file-name ".bookmarks.el" user-emacs-directory)
 buffers-menu-max-size 40
 case-fold-search t
 column-number-mode t
 compliation-scroll-output t
 delete-selection-mode t
 ediff-split-window-function 'split-window-horizontally
 edif-window-setup-function 'ediff-setup-windows-plain
 grep-hightlight-matches t
 grep-scroll-output t
 line-spacing 0.2
 macke-backup-files nil
 save-interprogram-paste-before-kill t
 set-mark-command-repeat-pop t
 tooltip-delay 1.5
 truncate-lines nil
 truncate-partial-width-windows nil
 visibile-bell t)

(global-auto-revert-mode)
(setq global-auto-revert-non-file-buffers t
      auto-revert-verbose nil)

(transient-mark-mode t)

(show-paren-mode 1)

(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-defun 'disabled nil)

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(provide 'init-custom-editing)

;;; init-custom-editing.el ends here
