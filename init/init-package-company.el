;;; init-package-company.el --- Adds some company

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file sets up the company-mode for Emacs, a text completion
;; framework. Company stands, of course, for "complete anything."

;;; Code:

(use-package company
  :ensure t
  :pin melpa-stable
  :config (global-company-mode)
  :diminish (company-mode . "C"))

(provide 'init-package-company)

;;; init-package-company.el ends here
