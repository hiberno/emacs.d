;;; init-language-csv.el --- CSV in Emacs

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file adds CSV to Emacs

;;; Code:

(use-package csv-mode
  :ensure t)

(use-package csv-nav
  :ensure t)

(provide 'init-language-csv)

;;; init-language-csv.el ends here
