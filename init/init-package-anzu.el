;;; init-package-anzu.el --- Anzu for Emacs

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file sets up the Emacs port of anzu.vim. Anzu provides
;; a minor mode which displays current match and total matches
;; information in the mode-line in various search modes.

;;; Code:

(use-package anzu
  :ensure t
  :pin melpa-stable
  :config (progn
	    (global-anzu-mode +1)
	    (global-set-key [remap query-replace-regexp] 'anzu-query-replace-regexp)
	    (global-set-key [remap query-replace] 'anzu-query-replace)
	    (setq anzu-mode-lighter ""
		  anzu-deactivate-region t
		  anzu-search-threshold 1000
		  azu-replace-to-string-separator "=> "))
  :diminish (anzu-mode . ""))

(provide 'init-package-anzu)

;;; init-package-anzu.el ends here
