;;; init-package-dired.el --- Managing files and directories with Emacs

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package dired
  :config (progn
	    (setq-default diredp-hide-details-initially-flag nil
			  dired-dwim-target t
			  direct-recursive-deletes 'top)
	    (when (fboundp 'global-dired-hide-details-mode)
	      (global-dired-hide-details-mode -1))
	    (use-package dired+
	      :ensure t)
	    (use-package dired-sort
	      :ensure t)))

(provide 'init-package-dired)

;;; init-package-dired.el ends here
