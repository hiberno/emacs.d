;;; init-package-flycheck.el --- Checking on the fly

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package flycheck
  :ensure t
  :pin melpa-stable
  :diminish (flycheck-mode . " ✓ ")
  :config (add-hook 'after-init-hook 'global-flycheck-mode))

(provide 'init-package-flycheck)

;;; init-package-flycheck.el ends here
