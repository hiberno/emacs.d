;;; init-package-dictcc.el --- Look it up at dict.cc

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file sets up dictcc, which enables dict.cc in Emacs.

;;; Code:

(use-package dictcc
  :ensure t
  :pin melpa
  :bind ("C-c C-d" . dictcc))

(provide 'init-package-dictcc)

;;; init-package-dictcc.el ends here
