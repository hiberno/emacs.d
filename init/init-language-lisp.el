;;; init-language-lisp --- Settings for editing LISP code

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package lisp-mode
  :config (add-hook 'emacs-lisp-mode-hook
		    (lambda ()
		      (setq mode-name " ξ ")
		      (smartparens-strict-mode +1))))

(provide 'init-language-lisp)

;;; init-language-lisp.el ends here
