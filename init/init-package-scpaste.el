;;; init-package-scpaste.el --- Pasting with Emacs

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package scpaste
  :ensure t
  :pin marmalade
  :config (setq
	   scppaste-http-destination "http://paste.hiberno.net"
	   scpaste-scp-destination "cargill@hiberno.net:~/paste"
	   scpaste-scp-port "21126"
	   scpaste-scp-pubkey "~/.ssh/id_rsa.pub"
	   scpaste-user-name "hiberno"
	   scpaste-user-address "http://hiberno.net"))

(provide 'init-package-scpaste)

;;; init-package-scpaste.el ends here
