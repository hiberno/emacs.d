;;; init-language-haskell --- Settings for editing Haskell code

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package haskell-mode
  :ensure t
  :pin melpa-stable
  :config (progn
	    (add-hook 'haskell-mode-hook (lambda () (smartparens-mode t)))
	    (use-package haskell-interactive-mode)
	    (use-package haskell-process
	      :config (setq haskell-program-name "nix-shell --command \"cabal repl\""
			    ghc-module-command "nix-shell --command \"ghc-mod\""
			    haskell-process-type 'cabal-repl
			    haskell-process-log t
			    haskell-process-wrapper-function (lambda (argv)
							       (append (list "nix-shell" "-I" "." "--command")
								       (list (mapconcat 'identity argv " "))))
			    haskell-ask-also-kill-buffers nil
			    haskell-interactive-popup-error t))
	    (use-package hi2
	      :ensure t
	      :pin melpa-stable
	      :config (add-hook 'haskell-mode-hook 'turn-on-hi2))
	    (use-package company-ghc
	      :ensure t
	      :pin melpa-stable
	      :config (progn
			(add-hook 'haskell-mode-hook 'company-mode)
			(add-to-list 'company-backends 'company-ghc)
			(custom-set-variables '(company-ghc-show-info t))))
	    (use-package haskell-flycheck
	      :load-path "additional-packages/haskell-flycheck/")
	    (bind-key "C-c C-o" 'haskell-compile haskell-mode-map)))

(provide 'init-language-haskell)

;;; init-language-haskell.el ends here
