;;; init-misc.el --- Miscellanous configurations and packagesn

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file contains miscellanous configurations and packages. Every
;; Emacs configuration needs a section like this.

;;; Code:

(use-package files
  :config (progn
	    (setq backup-directory-alist '(("." . "~/.emacs.d/backups"))
		  backup-by-copying t
		  version-control t
		  delete-old-versions t
		  kept-new-versions 20
		  kept-old-versions 5)))

(defalias 'yes-or-no-p 'y-or-n-p)

(setq browse-url-browser-function 'browse-url-generic
      browse-url-generic-program "chromium")

(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

(provide 'init-misc)

;;; init-misc.el ends here
