;;; init-mail --- Working with mails in Emacs with aid from notmuch

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file sets up notmuch.el, an Emacs interface to the notmuch
;; mail system.

;;; Code:

(defun hiberno/message-signature ()
  (let* ((sender (car (notmuch-clean-address (message-field-value "From"))))
	(file (cdr (assoc sender hiberno/signatures))))
    (with-temp-buffer
      (when (file-exists-p file)
	(insert-file-contents file))
      (buffer-string))))

(use-package notmuch
  :ensure t
  :load-path "/run/current-system/sw/share/emacs/site-lisp/"
  :config (progn
	    (setq notmuch-always-prompt-for-sender t
		  notmuch-crypto-process-mime t
		  notmuch-crypto-decryption t
		  message-kill-buffer-on-exit t
		  mail-specify-envelope-from t
		  message-sendmail-envelope-from 'header
		  mail-envelope-from 'header
		  message-send-mail-function 'message-send-mail-with-sendmail
		  sendmail-program "msmtp"
		  message-auto-save-directory "~/mail/drafts/"
		  notmuch-address-command "~/bin/nottoomuch-addresses.sh"
		  hiberno/signatures '(("christian.lask@bevuta.com" . "~/.emacs.d/private/bevuta.signature")
				    ("23a1a74@riseup.net" . "~/.emacs.d/private/23a1a74.signature")
				    ("mail@christianlask.de" . "~/.emacs.d/private/christian.signature")
				    ("mail@elfsechsundzwanzig.de" . "~/.emacs.d/private/_1126.signature")
				    ("business@christianlask.de" . "~/.emacs.d/private/christian.signature")
				    ("lists@elfsechsundzwanzig.de" . "~/.emacs.d/private/_1126.signature"))
		  message-signature #'hiberno/message-signature)
	    (setq notmuch-hello-sections
		  '(notmuch-hello-insert-saved-searches
		    notmuch-hello-insert-recent-searches
		    notmuch-hello-insert-alltags))
	    (notmuch-address-message-insinuate)
	    (setq notmuch-search-result-format
		  `(("date" . "%-15s")
		    ("authors" . " | %-20s")
		    ("subject" . " | %-45s")
		    ("tags" . " | (%s)")))
	    (setq notmuch-saved-searches '(("today" . "tag:today")
					   ("bevuta-unread" . "tag:bevuta and tag:unread")
					   ("bevuta-today" . "tag:bevuta and tag:today")
					   ("bevuta-tagesberichte" . "tag:bevuta-tagesberichte and tag:unread")
					   ("bevuta-redmine" . "tag:bevuta-redmine and tag:unread")
					   ("bevuta-gitlab" . "tag:bevuta-gitlab and tag:new")
					   ("bevuta-jira" . "tag:bevuta-jira and tag:unread")
					   ("christian" . "tag:christian and tag:unread")
					   ("business" . "tag:business and tag:unread")
					   ("_1126" . "tag:_1126 and tag:unread")
					   ("lists" . "tag:lists and tag:unread")
					   ("uni" . "tag:uni and tag:unread")
					   ("riseup" . "tag:riseup and tag:unread")
					   ("nix-dev" . "tag:nix-dev and tag:unread")
					   ("stumpwm" . "tag:stumpwm and tag:unread")
					   ("rust" . "tag:rust and tag:unread")
					   ("chicken-hackers" . "tag:chicken-hackers and tag:unread")
					   ("chicken-users" . "tag:chicken-users and tag:unread")
					   ("haskell-cafe" . "tag:haskell-cafe and tag:unread")
					   ("haskell-beginners" . "tag:haskell-beginners and tag:unread")
					   ("digitalbasics" . "tag:digitalbasics and tag:unread")
					   ("herbstluftwm" . "tag:hlwm and tag:unread")
					   ("freebsd-announce" . "tag:freebsd-announce and tag:unread")
					   ("freebsd-questions" . "tag:freebsd-questions and tag:unread")
					   ("freebsd-security" . "tag:freebsd-security and tag:unread")
					   ("freebsd-security-advisories" . "tag:freebsd-security-notifications and tag:unread")
					   ("smartos" . "tag:smartos and tag:unread")
					   ("clojure" . "tag:clojure and tag:clojure")))
	    (bind-key* "C-x n" 'notmuch)
	    (use-package notmuch-address)
	    (use-package gnus-art)))

(provide 'init-mail)

;;; init-mail.el ends here
