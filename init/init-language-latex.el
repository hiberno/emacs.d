;;; init-language-latex --- Settings for editing LaTeX

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package tex-site
  :ensure auctex
  :config (progn
	    (smartparens-mode +1)
	    (add-hook 'LaTeX-mode-hook
		      (lambda()
			(local-set-key [C-tab] 'TeX-complete-symbol)))))

(provide 'init-language-latex)

;;; init-language-latex.el ends here
