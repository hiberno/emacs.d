;;; init-package-uniquify --- Uniquify buffer names

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file sets up uniquify, a minor mode that uniquifies buffer
;; names if the corresponding file names are equal.

;;; Code:

(use-package uniquify
  :config (progn
	  (setq uniquify-buffer-name-style 'reverse)
	  (setq uniquify-separator " * ")
	  (setq uniquify-after-kill-buffer-p t)
	  (setq uniquify-ignore-buffers-re "^\\*")))

(provide 'init-package-uniquify)

;;; init-package-uniquify.el ends here
