;;; init-package-magit --- It's Magit.

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file sets up the one and only Magit.

;;; Code:

(use-package magit
  :ensure t
  :pin melpa-stable
  :bind (("C-x g s" . magit-status)
	 ("C-x g l" . magit-log)
	 ("C-x g f" . magit-file-log)
	 ("C-x g b" . magit-blame-mode))
  :config (progn
	    (setq magit-last-seen-setup-instructions "1.4.0")
	    (defadvice magit-status (around magit-fullscreen activate)
	      (window-configuration-to-register :magit-fullscreen)
	      ad-do-it
	      (delete-other-windows))
	    (use-package gitconfig-mode
	      :ensure t)
	    (use-package gitignore-mode
	      :ensure t)
	    (use-package git-messenger
	      :ensure t
	      :bind ("C-x g m" . git-messenger:popup-message))))

(provide 'init-package-magit)

;;; init-package-magit.el ends here
