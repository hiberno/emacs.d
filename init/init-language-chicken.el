;;; init-language-chicken --- Settings for editing Chicken code

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(defun hiberno/run-scheme (scheme-program)
  "Run scheme with SCHEME-PROGRAM if it is not already running."
  (unless (get-buffer "*scheme*")
    (progn
      (setq debug-on-error t)
      (message "Loading *scheme*")
      (run-scheme scheme-program))))

(defun hiberno/scheme-send-definition-and-go ()
  "Send entire content of the buffer to the Inferior Scheme process.
Go to the buffer afterwards."
  (interactive)
  (scheme-send-region-and-go (point-min) (point-max)))

(defun hiberno/query-chicken-doc ()
  "Ask `chicken-doc' for help."
  (interactive)
  (let ((func (current-word)))
    (when func
      (process-send-string "*scheme*" (format "(use chicken-doc) ,doc %S\n" func))
      (switch-to-buffer "*scheme*"))))

(defun hiberno/scheme-mode ()
  (setq scheme-program-name "~/bin/chicken/bin/csi -n")
  (add-to-list 'load-path "~/bin/chicken/lib/chicken/6/")
  (smartparens-strict-mode +1)
  (bind-key "C-c C-r" (lambda ()
                        (interactive)
                        (hiberno/run-scheme scheme-program-name)) scheme-mode-map)
  (bind-key "C-c C-b" 'hiberno/scheme-send-definition-and-go scheme-mode-map)
  (bind-key "C-c C-d" 'hiberno/query-chicken-doc))

(add-to-list 'auto-mode-alist '("\\.sxml\\'" . scheme-mode))
(add-to-list 'auto-mode-alist '("\\.scss\\'" . scheme-mode))

(add-hook 'scheme-mode-hook 'hiberno/scheme-mode)

(provide 'init-language-chicken)

;;; init-language-chicken.el ends here
