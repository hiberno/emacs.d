;;; init-package-deft.el --- Work with plaintext files directories

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package deft
  :ensure t
  :load-path "~/.emacs.d/additional-packages/deft-turbo/"
  :bind ("<f8>" . deft)
  :config (progn
	    (setq deft-directory (expand-file-name "~/org/notes/")
		  deft-extensions '("md" "org")
		  deft-default-extension "org"
		  deft-text-mode 'org-mode
		  deft-use-filename-as-title t)))

(provide 'init-package-deft)

;;; init-package-deft.el ends here
