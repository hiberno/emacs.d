;;; init-package-elfeed.el --- RSS/Atom for Emacs

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file sets up the elfeed, a notmuch inspired RSS/Atom feed
;; reader for Emacs.

;;; Code:

(use-package elfeed
  :ensure t
  :config (setq elfeed-feeds
		'(("https://blog.fefe.de/rss.xml" politik nachrichten)
		  ("https://netzpolitik.org/feed" politik nachrichten)
		  ("http://www.metronaut.de/feed" politik)

		  ("https://derzaunfink.wordpress.com/feed/" politik gender feminismus)
		  
		  ("http://fokus-fussball.de/feed/" fußball)
		  ("http://www.trainer-baade.de/feed/" fußball)
		  ("http://www.fussballwatchblog.com/feed/" fußball)
		  ("http://www.schwatzgelb.de/feed-de.rss" fußball bvb)
		  ("http://www.miasanrot.de/feed/" fußball bayern)
		  ("http://feeds2.feedburner.com/wordpress/angedacht" fußball vfb)
		  ("https://anygivenweekend.wordpress.com/feed/" fußball bvb)
		  ("http://feeds.feedburner.com/koenigsblog" fußball s04)
		  ("http://spielverlagerung.de/rss/" fußball taktik)
		  ("http://www.neureich-bimbeshausen.de/rss.xml" fußball tsg)

		  ("http://www.wochenendrebell.de/feed/" dieses&jenes)
		  ("http://dembowskiermittelt.blogspot.com/feeds/posts/default" dieses&jenes)
		  ("http://www.wortpiratin.de/blog/?feed=rss2" dieses&jenes)

		  ("http://blog.rust-lang.org/feed.xml" programming)
		  ("http://www.more-magic.net/feed.atom" programming)
		  ("http://ceaude.twoticketsplease.de/feed.atom" programming)
		  ("http://words.steveklabnik.com/feed" programming politics)
		  ("http://feeds.feedburner.com/codinghorror" programming)
		  ("http://planet.clojure.in/atom.xml" programming)
		  ("http://feeds2.feedburner.com/stevelosh" programming)
		  ("http://blog.fogus.me/feed/" programming)
		  ("https://pcwalton.github.io/atom.xml" programming)
		  ("http://steve-yegge.blogspot.com/atom.xml" programming)U
		  ("http://paulgraham.com/rss.html" programming)
		  ("http://lambda-the-ultimate.org/rss.xml" programming)
		  ("http://www.codesimplicity.com/feed/atom/" programming)
		  ("http://prog21.dadgum.com/atom.xml" programming)
		  ("http://blog.burntsushi.net/index.xml" programming))
		url-queue-timeout 60)
  :bind ("C-x ö" . elfeed))

(provide 'init-package-elfeed)

;;; init-package-elfeed.el ends here
