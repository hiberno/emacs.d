;;; init-package-rainbow-delimiters.el --- Higlight delimiters according to their depth

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package rainbow-delimiters
  :ensure t
  :pin melpa-stable
  :diminish (rainbow-delimiters-mode . "")
  :config (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

(provide 'init-package-rainbow-delimiters)

;;; init-package-rainbow-delimiters.el ends here
