;;; init-package-highlight-symbol.el --- Highlight symbols in buffer

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package highlight-symbol
  :ensure t
  :pin melpa-stable
  :config (progn
	    (add-hook 'prog-mode-hook 'highlight-symbol-mode))
  :diminish highlight-symbol-mode)

(provide 'init-package-highlight-symbol)

;;; init-package-hightlight-symbol.el ends here
