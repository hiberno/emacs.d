;;; init-package-unfill.el --- Unfilling

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file sets up the unfill mode by Purcell. unfill provides
;; the inverse of Emacs' fill-paragraph and fill-region.

;;; Code:

(use-package unfill
  :pin marmalade
  :ensure t)

(provide 'init-package-unfill)

;;; init-package-unfill.el ends here
