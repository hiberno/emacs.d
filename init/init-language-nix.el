;;; init-language-nix --- Settings for editing Nix code

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package nix-mode
  :ensure t
  :pin melpa-stable
  :init (progn
	  (push '("\\.nix\\'" . nix-mode) auto-mode-alist)
	  (push '("\\.nix.in\\'" . nix-mode) auto-mode-alist)
	  (add-hook 'nix-mode-hook (lambda () (smartparens-strict-mode t)))))

(provide 'init-language-nix)

;;; init-language-nix.el ends here
