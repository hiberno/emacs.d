;;; init-package-weechat.el --- Weechat for Emacs

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file sets up weechat.el - a mode to make Emacs talk to a
;; weechat relay.

;;; Code:

(use-package weechat
  :ensure t
  :pin melpa-stable
  :init (progn
	  (setq weechat-host-default "flume"
		weechat-port-default 60012
		weechat-mode-default "ssh -W localhost:%p %h"
		weechat-auto-monitor-buffers t
		weechat-sync-active-buffer t
		weechat-modules '(weechat-button
				  weechat-complete
				  weechat-tracking)
		weechat-tracking-types '(:highlight
					 (".*f0o.+" . :message)
					 (".*hiberno.*" :message))
		weechat-tracking-show-buffer-predicate (lambda (message-type buffer)
							 (and buffer (eq :private (weechat-buffer-type buffer))))
		weechat-prompt "> ")
	  (tracking-mode)
	  (bind-key* "C-," 'weechat-monitor-buffer)
	  (bind-key* "C-." 'weechat-switch-buffer)))

(provide 'init-package-weechat)

;;; init-package-weechat.el ends here
