;;; init-package-flyspell.el --- Spellchecking on the fly

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package flyspell
  :ensure t
  :diminish (flyspell-mode . " FS ")
  :bind ("C-c M-i" . ispell-word))

(provide 'init-package-flyspell)

;;; init-package-flyspell.el ends here
