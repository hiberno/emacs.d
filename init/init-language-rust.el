;;; init-language-rust --- Settings for editing Rust code

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package rust-mode
  :ensure t
  :pin melpa-stable
  :init (progn
	  (use-package flycheck-rust
	    :ensure t
	    :pin melpa-stable)
	  (add-hook 'rust-mode-hook (lambda () (smartparens-strict-mode t)))
	  (add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))))

(provide 'init-language-rust)

;;; init-language-rust.el ends here
