;;; init-package-define-word.el --- Define word at point

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;; This file sets up define word, which, well, defines a word at point

;;; Code:

(use-package define-word
  :ensure t
  :pin melpa
  :bind (("C-c d" . define-word-at-point)
	 ("C-c D" . define-word)))

(provide 'init-package-define-word)

;;; init-package-define-word.el ends here
