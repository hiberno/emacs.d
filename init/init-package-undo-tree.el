;;; init-package-undo-tree.el --- Undo the right way.

;; Copyright (c) 2015 hiberno (Christian Lask)
;; Version: 1.0
;; URL: https://github.com/hiberno/emacs.d

;;; Commentary:

;;; Code:

(use-package undo-tree
  :ensure t
  :diminish ""
  :config (progn
	 (global-undo-tree-mode))
  :diminish (undo-tree-mode . ""))

(provide 'init-package-undo-tree)

;;; init-package-undo-tree.el ends here
